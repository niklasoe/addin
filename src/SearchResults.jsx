import * as React from 'react';
import {Button} from 'office-ui-fabric-react';
import { Link } from "react-router-dom";
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { Image } from 'office-ui-fabric-react/lib/Image';
import Helper from './helper.js'
import './searchResult.scss';

class SearchResults extends React.Component {
    componentWillMount() {
        Office.initialize = function (reason) {
            
          };
    }

    render() {
        var searchResults = this.props.items.map((item, index) => {
            return (
                <Link className="video-link" to={"/video/" + item._id}>
                    <div className='video-item' style={{display: "flex"}}>
                        <div className='img-wrapper' style={{width: 75, height: 75}}>
                            <Image src={item.chapterImg} alt='Example implementation with no image fit property and only width is specified.' width={ 75 } />
                        </div>
                        
                        <div className='search-result text' style={{display: "flex", flexDirection: "column"}}>
                            <div className='title'>{item.title}</div>
                            <div className='duration'>{Helper.formatTime(item.duration, false, true)}</div>
                        </div>
                    </div>
                </Link>
                
            )
        })
        return (    
            <div className="Search-Results">
                {searchResults}
            </div>
        );
    }
}

export default SearchResults;
