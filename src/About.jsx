import * as React from 'react';
import {Button} from 'office-ui-fabric-react';
import { Link } from "react-router-dom";
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import Search from './Search.jsx'

import './App.scss';

class About extends React.PureComponent {
    componentWillMount() {
        Office.initialize = function (reason) {
            
          };
          console.log(JSON.parse(localStorage.getItem('user')))
    }

    render() {
        return (    
            <div className="App">
                <div className="ms-Grid">
                    <div className="ms-Grid-row">
                        <div className="ms-Grid-col ms-u-sm12 ms-u-md12 ms-u-lg12">
                            
                            <Search />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default About;
