import * as React from 'react';
import { Button } from 'office-ui-fabric-react';
import { Link, withRouter } from "react-router-dom";
import $ from 'jquery'
import './App.scss';
import { DefaultButton, IButtonProps } from 'office-ui-fabric-react/lib/Button';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import {
    Spinner,
    SpinnerSize
  } from 'office-ui-fabric-react/lib/Spinner';

class Home extends React.Component {
    constructor(props, context) {
        super(props, context);
    
        // set the initial component state
        this.state = {
            email: "",
            password: "",
            loading: false
        }
        this._login = this._login.bind(this)
      }
    componentWillMount() {

        Office.initialize = function (reason) {
            
          };
          
          if ( localStorage.getItem('user') != null ) {
              this.props.history.push("/about")
          }

    }
    
    _login() {
        var that = this
        this.setState({loading: true})
        $.ajax({
            type: "GET",
            dataType: 'jsonp',
            url: 'https://learningserver.herokuapp.com/auth/login/jsonp?email=' + that.state.email + '&password=' + that.state.password + '&callback=?',                     
            success: function(res) {
                 localStorage.setItem('user', JSON.stringify(res.user))     
                 that.setState({loading: false})
                 that.props.history.push("/about")
            }})
    }
  
    render() {
        return (
            <div className="App">
                <div className="ms-Grid">
                    <div className="ms-Grid-row">
                    {!this.state.loading &&
                        
                        <div className="ms-Grid-col ms-u-sm12 ms-u-md12 ms-u-lg12">
                                <h2>
                                Login
                                </h2>
                                <TextField placeholder='Email' onChanged={(newValue) => { this.setState({email: newValue})}} />
                                <TextField placeholder='Password' type="password" onChanged={(newValue) => { this.setState({password: newValue})}} />
                                <DefaultButton
                                    primary={ true }
                                    data-automation-id='test'
                                    disabled={ false }
                                    text='Button'
                                    onClick={ this._login }
                                >Login</DefaultButton>
                                <a href="https://learningserver.herokuapp.com/">Sign Up</a>
                        </div>
                        
                            }
                            {this.state.loading &&
                                <div className='' style={{width: "100%", height: "100vh", position: "relative"}}>
                                    <Spinner size={ SpinnerSize.large } style={{margin: "auto"}}/>
                                </div>
                            }
                            
                    </div>
                </div>
            </div>
        );
    }
}
export default withRouter(Home);
