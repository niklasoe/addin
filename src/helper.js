class Helper {
  /**
   * Format time
   *
   * @param {number} time in sec
   */
  static formatTime(time, showHours, showSec) {
    if (typeof time == 'undefined') {
      return ""
    }

    var hours = parseInt(time / 3600);
    if (time > 360000) {
      return hours + " Hours"
    }
    time -= hours * 3600;
    var min = parseInt(time / 60);
    time -= min * 60;
    var sec = time;
    if (showHours) {
      if (!showSec) {
        return hours + " Hr " + min + " Min";
      }
      return hours + " Hr " + min + " Min " + sec + " Sec";
    } else {
      return min + " Min " + sec + " Sec";
    }
  }
  static shortFormatTime(time, showHours, showSec) {
    if (typeof time == 'undefined') {
      return ""
    }

    var hours = parseInt(time / 3600);
    if (time > 360000) {
      return hours + " Hr"
    }
    time -= hours * 3600;
    var min = parseInt(time / 60);
    time -= min * 60;
    var sec = time;
    if (showHours) {
      if (!showSec) {
        return hours + "H " + min + "Min";
      }
      return hours + "H " + min + "Min " + sec + "Sec";
    } else {
      return min + "Min " + sec + "Sec";
    }
  }
}

export default Helper;
