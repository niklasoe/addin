import * as React from 'react';
import {Button} from 'office-ui-fabric-react';
import { Link } from "react-router-dom";
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import Search from './Search.jsx'
import { Icon } from 'office-ui-fabric-react/lib/Icon';
import './App.scss';

class Video extends React.PureComponent {
    constructor(props, context) {
        super(props, context);
    
        // set the initial component state
        this.state = {
            video: {
                title: "",
                description: "",
                videoURL: ""
            },
        }
        this._back = this._back.bind(this)
      }
    componentWillMount() {
        Office.initialize = function (reason) {
            
          };
          console.log(JSON.parse(localStorage.getItem('user')))
    }
    _back() {
        this.props.history.goBack()
    }
    render() {
        var items = JSON.parse(localStorage.getItem('searchResult'))
        var video = {}
        for (var i in items) {
            if (items[i]._id == this.props.match.params.id) {
                video = items[i]
            }
        }
        return (    
            <div className="App">
            
            {video.title != '' &&
            <div className='video'>
                <div className='top-bar' style={{display: "flex", marginBottom: 10}}>
                    <Icon iconName={ 'Back' } onClick={this._back}/>
                    <div className='title'>{video.title}</div>
                </div>
                <video controls>
                    <source src={video.videoURL} type="video/mp4" />
                </video>
                <div className='' style={{display: "flex", flexDirection: "column", paddingLeft: 10, paddingRight: 10}}>
                    <h3>Description</h3>
                    <div className='description'>{video.description}</div>
                </div>
                
            </div>
            
        }
                
            </div>
        );
    }
}

export default Video;
