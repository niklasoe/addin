import * as React from 'react';
import { Link } from "react-router-dom";
import { DefaultButton, IButtonProps } from 'office-ui-fabric-react/lib/Button';
import $ from 'jquery'
import SearchResults from './SearchResults.jsx'
import './App.scss';
import { SearchBox } from 'office-ui-fabric-react/lib/SearchBox';
import {
    Spinner,
    SpinnerSize
  } from 'office-ui-fabric-react/lib/Spinner';
class Search extends React.Component {
    constructor(props, context) {
        super(props, context);
    
        // set the initial component state
        this.state = {
            searchVal: "",
            items: [],
            loading: false
        }
        
        this.change = this.change.bind(this)
        this._onSearch = this._onSearch.bind(this)
    }

    componentWillMount() {
        Office.initialize = function (reason) {
            
          };
    }
    _onSearch() {
        var that = this
        this.setState({loading: true})
        $.ajax({
            type: "GET",
            dataType: 'jsonp',
            url: 'https://learningserver.herokuapp.com/search/jsonp?searchTerm=' + this.state.searchVal + '&callback=?',                     
            success: (res) => {
                localStorage.setItem('searchResult', res.items)
                that.setState({items: JSON.parse(res.items), loading: false})
            }
        })
    }
    change(newValue) {
        this.setState({searchVal: newValue}, () => {
            console.log(this.state.searchVal)
        })
    }
    render() {
        return (  
            <div style={{display: "flex", flexDirection: 'column'}}>
                <div className="Search-bar" style={{display: "flex", width: "100%"}}>
                    <SearchBox
                    // tslint:disable:jsx-no-lambda
                        onChange={this.change}
                        onSearch={this._onSearch}
                        onClear={() => { this.setState({searchVal: ""}) }}
                    />
                </div>
                {!this.state.loading && 
                    <SearchResults items={this.state.items} />
                }
                
                {this.state.loading &&
                    <div className='' style={{width: "100%", height: "100vh", position: "relative"}}>
                        <Spinner size={ SpinnerSize.large } style={{margin: "auto"}}/>
                    </div>
                }
            </div>  
            
        );
    }
}

export default Search;
