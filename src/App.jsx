import * as React from 'react';
import {Button} from 'office-ui-fabric-react';
import Home from './Home';
import About from './About';
import { HashRouter, Switch, Route } from 'react-router-dom'
import Video from './Video'
import './App.scss';

class App extends React.Component {
    componentWillMount() {
        var pathname = window.location.pathname; 
        console.log(pathname)
        Office.initialize = function (reason) {
            
          };
    }

    render() {
        return (
            <HashRouter>
                <Switch>
                    <Route exact path='/index.html' component={Home}/>
                    <Route exact path='/' component={Home}/>
                    <Route path='/about' component={About}/>
                    <Route path='/video/:id' component={Video} render={(props) => { <Video params={props.params} />}}/>
              </Switch>
            </HashRouter>
        );
    }
}

export default App;
