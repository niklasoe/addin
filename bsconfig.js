module.exports = {
    "ui": {
        "port": process.env.PORT || 3000
    },
    "server": {
        "baseDir": "dist",
        "routes": {
            "/node_modules": "node_modules"
        }
    },
    "https": true,
    "watch": true,
    "open": false,
    "files": "dist/**/*"
}